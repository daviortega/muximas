interface Iitem {
  name: string
  price: number
  description: string
  image: string
}

interface ISection {
  name: string
  show: boolean
  items: Iitem[]
}

interface IMenu {
  name: string
  show?: boolean
  items: ISection[]
}

export {
  Iitem,
  ISection,
  IMenu
}
